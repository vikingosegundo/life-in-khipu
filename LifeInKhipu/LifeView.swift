//
//  LifeView.swift
//  DeclarationOfLife
//
//  Created by vikingosegundo on 06/03/2022.
//

import SwiftUI
//MARK: - public
struct LifeView {
    let timer = Timer.publish(every: 1.0/200.0, on: .main, in: .common).autoconnect()
    let cellWidth      : CGFloat
    let cellHeight     : CGFloat
    let backgroundColor: Color
    let foregroundColor: Color
    @EnvironmentObject
    private var viewState: ViewState
}
extension LifeView: View {
    var body: some View {
        GeometryReader { geo in
            CellBoard( rows: Int(geo.size.height / cellHeight),
                    columns: Int(geo.size.width  / cellWidth ),
                  cellWidth: cellWidth,
                 cellHeight: cellHeight
            )
            .background(backgroundColor)
            .foregroundColor(foregroundColor)
            .frame(width: geo.size.width,
                  height: geo.size.height,
               alignment: .center
            )
        }.onReceive(timer) { _ in viewState.roothandler(.life(.process)) }
    }
}
//MARK: - private
fileprivate
extension LifeView {
    struct CellBoard {
        let rows      : Int
        let columns   : Int
        let cellWidth : CGFloat
        let cellHeight: CGFloat
        @EnvironmentObject
        private var viewState: ViewState
    }
}
extension LifeView.CellBoard: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        for row in 0 ..< rows {
            for column in 0 ..< columns {
                if stateFor(coordinate: .init(x: column, y: row), in: viewState) == .alive {
                    let rect = CGRect(x: cellWidth * CGFloat(column), y:cellHeight * CGFloat(row), width:cellWidth, height: cellHeight)
                    path.addRect(rect.insetBy(dx: 1, dy: 1))
                }
            }
        }
        return path
    }
}
