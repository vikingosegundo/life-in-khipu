//
//  ViewState.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI

final class ViewState: ObservableObject {
    @Published public var stateForCoordiantes: [Life.Cell.Coordinate:Life.Cell.State] = [:]
    @Published var roothandler         : (Message) -> ()
    @Published var numberAliveCells:Int = 0
    @Published var numberSteps:Int = 0

    init(store: Store, roothandler: @escaping (Message) -> ()) {
        self.roothandler = roothandler
        store.updated { self.process(store) }
        process( store)
    }

    func process(_ store: Store) {
        DispatchQueue.main.async {
            self.stateForCoordiantes = store.state().life.stateForCoordiantes
            self.numberAliveCells = store.state().life.stateForCoordiantes.count
            self.numberSteps = store.state().life.step
        }
    }
}
