//
//  StateHandler.swift
//  Todos
//
//  Created by Manuel Meyer on 12/04/2021.
//

typealias Access   = (                    ) -> AppState
typealias Change   = ( AppState.Change... ) -> ()
typealias Reset    = (                    ) -> ()
typealias Callback = ( @escaping () -> () ) -> ()
typealias Destroy  = (                    ) -> ()

typealias Store = (  state: Access,
            change: Change,
            reset: Reset,
            updated: Callback,
            destroy: Destroy )

func state  (in store:Store                        ) -> AppState { store.state()                   }
func change (_  store:Store,_ cs:AppState.Change...)             { cs.forEach { store.change($0) } }
func reset  (_  store:Store                        )             { store.reset();                  }
func destroy(_  store:inout Store!                 )             { store.destroy(); store = nil    }
