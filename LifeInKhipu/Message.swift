//
//  Message.swift
//  LifeInKhipu
//
//  Created by vikingosegundo on 08/03/2022.
//

enum Message {
    case life(_Life)        ; enum _Life {
        case process
        case start(_Start)  ; enum _Start {
            case game
        }
        case started
    }
}
