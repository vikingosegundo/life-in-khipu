//
//  LifeInKhipuApp.swift
//  LifeInKhipu
//
//  Created by vikingosegundo on 08/03/2022.
//

import SwiftUI

@main
final class LifeInKhipuApp: App {
    private      let store      : Store           = createDiskStore()
    private lazy var viewState  : ViewState       = ViewState(store:store, roothandler: { self.rootHandler($0) })
    private lazy var rootHandler: (Message) -> () = createAppDomain(
        store      : store,
        receivers  : [ ],
        rootHandler: { self.rootHandler($0) }
    )
    var body: some Scene {
        WindowGroup {
            ContentView(viewState: viewState)
        }
    }
}
