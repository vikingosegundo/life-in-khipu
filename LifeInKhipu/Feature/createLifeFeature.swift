//
//  createLifeFeature.swift
//  LifeInKhipu
//
//  Created by vikingosegundo on 08/03/2022.
//

func createLifeFeature(store: Store, output: @escaping Output) -> Input {
    let starter   = Life.Starter  (store:store) { res in switch res { case .started: output(.life(.started)) } }
    let processor = Life.Processor(store:store) { res in                                                       }
    
    func execute(command cmd:Message._Life) {
        if case .start(.game) = cmd { starter  .request(.start  ) }
        if case .process      = cmd { processor.request(.process) }
    }
    return { // entry point
        if case .life(let c) = $0 { execute(command:c) } // execute if life feature is command's recipient
    }
}
func gliderGun(offset c:Life.Cell.Coordinate = .init(x: 0, y: 0)) -> [Life.Cell.Coordinate] {
    [
        .init(x:c.x   , y:c.y+4),
        .init(x:c.x+01, y:c.y+4),
        .init(x:c.x   , y:c.y+5),
        .init(x:c.x+01, y:c.y+5),
        .init(x:c.x+34, y:c.y+3),
        .init(x:c.x+35, y:c.y+3),
        .init(x:c.x+34, y:c.y+4),
        .init(x:c.x+35, y:c.y+4),
        .init(x:c.x+10, y:c.y+4),
        .init(x:c.x+10, y:c.y+5),
        .init(x:c.x+10, y:c.y+6),
        .init(x:c.x+11, y:c.y+3),
        .init(x:c.x+11, y:c.y+7),
        .init(x:c.x+12, y:c.y+2),
        .init(x:c.x+13, y:c.y+2),
        .init(x:c.x+12, y:c.y+8),
        .init(x:c.x+13, y:c.y+8),
        .init(x:c.x+14, y:c.y+5),
        .init(x:c.x+15, y:c.y+3),
        .init(x:c.x+15, y:c.y+7),
        .init(x:c.x+16, y:c.y+4),
        .init(x:c.x+16, y:c.y+5),
        .init(x:c.x+16, y:c.y+6),
        .init(x:c.x+17, y:c.y+5),
        .init(x:c.x+20, y:c.y+2),
        .init(x:c.x+21, y:c.y+2),
        .init(x:c.x+20, y:c.y+3),
        .init(x:c.x+21, y:c.y+3),
        .init(x:c.x+20, y:c.y+4),
        .init(x:c.x+21, y:c.y+4),
        .init(x:c.x+22, y:c.y+1),
        .init(x:c.x+22, y:c.y+5),
        .init(x:c.x+24, y:c.y+1),
        .init(x:c.x+24, y:c.y+5),
        .init(x:c.x+24, y:c.y  ),
        .init(x:c.x+24, y:c.y+6),
    ]
}
