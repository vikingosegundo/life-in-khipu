//
//  Life.swift
//  DeclarationOfLife
//
//  Created by vikingosegundo on 05/03/2022.
//

import Foundation
//MARK: - public
struct Life:Codable {
    enum Change {
        case process
        case pause, unpause
        case set (_Set); enum _Set {
            case cells([Cell])
        }
    }
    struct Cell: Hashable, Equatable, Codable {
        init(coordinate: Coordinate, state:State = .alive) {
            self.coordinate = coordinate
            self.state = state
        }
        let coordinate: Coordinate; struct Coordinate: Equatable, Hashable, Codable {  let x, y: Int   }
        let state     : State;        enum State     :            Hashable, Codable { case alive, dead }
    }
    init(coordinates: [Cell.Coordinate]) { self.init(cells:coordinates.map { Cell(coordinate:$0) }) }
    init(cells      : [Cell           ]) { self.init(0,cells,false)           }
    func alter(_ cs: [Change] ) -> Self { cs.reduce(self) { $0.alter($1) } }
    func alter(_ cs: Change...) -> Self { cs.reduce(self) { $0.alter($1) } }
    func alter(_ c : Change   ) -> Self {
        if paused && !unpausing(c) { return self }
        switch c {
        case let .set(.cells(cells)): return .init(step+1,cells                                                    ,paused)
        case                  .pause: return .init(step+1,stateForCoordiantes.map{ Cell(coordinate: $0, state: $1)},  true)
        case                .unpause: return .init(step+1,stateForCoordiantes.map{ Cell(coordinate: $0, state: $1)}, false)
        case                .process: return alter(.set(.cells(applyRules())))
        }
    }
    func debug(_ exe: (Self) -> ()) -> Self { exe(self); return self }
    let paused : Bool
    let step               : Int
    let stateForCoordiantes: [Life.Cell.Coordinate:Life.Cell.State]
}
//MARK: - private
private extension Life {
    init(_ step: Int,_ cells: [Cell] = [],_ paused:Bool) {
        self.step                = step
        self.paused              = paused
        self.stateForCoordiantes = cells.reduce([:]) { var a = $0; a[$1.coordinate] = $1.state; return a }
    }
    func applyRules() -> [Life.Cell] {
        Array(Set(self.stateForCoordiantes.map { k,v in Cell(coordinate: k, state: v)}.flatMap { neighbors(for:$0) }))
            .compactMap {
                let neigbours = aliveNeighbors(for: $0)
                let isAlive:Bool
                switch (neigbours.count, $0.state) {
                case (0...1, .alive): isAlive = false
                case (2...3, .alive): isAlive = true
                case (4...8, _     ): isAlive = false
                case (3,      .dead): isAlive = true
                case (_,      _    ): isAlive = false
                }
                return isAlive ? .init(coordinate: .init(x: $0.coordinate.x, y: $0.coordinate.y), state:.alive) : nil
            }
    }
    func neighbors(for cell: Life.Cell) -> [Life.Cell] {
        aliveNeighbors(for: cell) + deadNeighbors(for: cell)
    }
    func aliveNeighbors(for cell: Life.Cell) -> [Life.Cell] {
        allNeigbourCoordinates(cell).map {
            Cell(coordinate: $0, state: stateForCoordiantes[$0] ?? .dead)
        }.filter { $0.state == .alive }
    }
    func allNeigbourCoordinates(_ cell: Life.Cell) -> [Life.Cell.Coordinate] {
        [
            .init(x:cell.coordinate.x-1, y:cell.coordinate.y-1),
            .init(x:cell.coordinate.x-1, y:cell.coordinate.y  ),
            .init(x:cell.coordinate.x-1, y:cell.coordinate.y+1),
            .init(x:cell.coordinate.x  , y:cell.coordinate.y-1),
            .init(x:cell.coordinate.x  , y:cell.coordinate.y+1),
            .init(x:cell.coordinate.x+1, y:cell.coordinate.y-1),
            .init(x:cell.coordinate.x+1, y:cell.coordinate.y  ),
            .init(x:cell.coordinate.x+1, y:cell.coordinate.y+1),
        ]
    }
    func deadNeighbors(for cell: Life.Cell) -> [Life.Cell] {
        let alive = aliveNeighbors(for: cell)
        let dead = Set(allNeigbourCoordinates(cell)).subtracting(alive.map{$0.coordinate}).map{ Cell(coordinate: $0, state: .dead) }
        return dead
    }
}
private func unpausing(_ c:Life.Change) -> Bool {
    switch c {
    case .unpause: return true
    default      : return false
    }
}
