//
//  Processor.swift
//  LifeInKhipu
//
//  Created by vikingosegundo on 08/03/2022.
//

import Foundation

extension Life {
    struct Processor:UseCase {
        enum Request { case process }
        enum Response {case processed }
        init(store s: Store, responder r: @escaping (Response) -> ()) {
            store = s
            repond = r
        }
        let store:Store
        let repond:(Response) -> ()
        func request(_ request: Request) {
            switch request {
            case .process:
                DispatchQueue.global(qos: .userInitiated).async {
                    store.change(.replace(store.state().life.alter(.process)))
                    DispatchQueue.main.async {
                        repond(.processed)
                    }
                }
            }
        }
        typealias RequestType = Request
        typealias ResponseType = Response
    }
}
