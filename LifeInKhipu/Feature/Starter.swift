//
//  Starter.swift
//  LifeInKhipu
//
//  Created by vikingosegundo on 08/03/2022.
//

extension Life {
    struct Starter: UseCase {
        enum Request { case start }
        enum Response {case started}
        init(store s: Store, responder r: @escaping (Response) -> ()) {
            store = s
            repond = r
        }
        let store:Store
        let repond:(Response) -> ()
        func request(_ requesr: Request) {
            switch requesr {
            case .start:
                store.change(.start(Life(coordinates: gliderGun())))
                repond(.started)
                
            }
        }
        typealias RequestType = Request
        typealias ResponseType = Response
    }
}
