//
//  AppState.swift
//  LifeInKhipu
//
//  Created by vikingosegundo on 08/03/2022.
//

struct AppState:Codable {
    enum Change {
        case start(Life)
        case replace(Life)
    }
    
    func alter(_ changes: [Change] ) -> Self { changes.reduce(self) { $0.alter($1) } }
    func alter(_ changes: Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    
    func alter(_ change:Change) -> Self {
        switch change {
        case .start(let life): return .init(life: life)
        case .replace(let life): return .init(life: life)
            
        }
    }
    
    init(){
        self.init(life: Life(coordinates:gliderGun()))
    }
    
    private init(life l:Life) {
        life = l
    }
    
    let life: Life
}
