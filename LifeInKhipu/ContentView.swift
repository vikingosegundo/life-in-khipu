//
//  ContentView.swift
//  LifeInKhipu
//
//  Created by vikingosegundo on 08/03/2022.
//

import SwiftUI

struct ContentView: View {
    init(viewState: ViewState) {
        self.viewState   = viewState
        self.roothandler = viewState.roothandler
    }
    public var body: some View {
        VStack {
            Text("Currently living cells:\(viewState.numberAliveCells) / Generation \(viewState.numberSteps)").monospacedDigit()
            ZStack {
                LifeView(cellWidth:4, cellHeight:4, backgroundColor: .black, foregroundColor: .green)
                    .environmentObject(viewState)
                VStack {
                    Spacer()
                    HStack {
                        Spacer()
                        Button {                  } label: { Text("reset")                                }
                        Button {                  } label: { Text( "pause") }
                        Spacer()
                    }.padding().background(Color.black.opacity(0.7))
                }
            }
        }
    }
    @ObservedObject
    private var viewState  : ViewState
    var roothandler: (Message) -> ()
}
